import React from 'react';

const AddButton = ({ onClick }) => {
    return (
        <button type="button" className="add-button" onClick={onClick}>
            +
        </button>
    );
};

export default AddButton;
