import React from 'react';
import PropTypes from 'prop-types';

const Title = ({ title, handleOnChange, handleDraggableChange }) => {
    return (
        <input
            type="text"
            className="title"
            name="title"
            value={title}
            onChange={handleOnChange}
            onMouseDown={(e) => {
                e.stopPropagation();

                handleDraggableChange(false);
            }}
        />
    );
};

Title.propTypes = {
    title: PropTypes.string.isRequired,
    handleDraggableChange: PropTypes.func.isRequired,
    handleOnChange: PropTypes.func.isRequired,
};

export default Title;
