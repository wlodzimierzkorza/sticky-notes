import React from 'react';

const Menu = ({ handleColorChange }) => {
    const colors = ['yellow', 'red', 'blue', 'purple', 'green'];
    return (
        <div className="menu">
            {colors.map((color) => {
                return (
                    <button
                        type="button"
                        label={color}
                        className={color}
                        onClick={() => handleColorChange(color)}
                        key={color}
                    />
                );
            })}
        </div>
    );
};

export default Menu;
