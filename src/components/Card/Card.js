import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateItem, removeItem } from '../../redux/actions';
import Title from './Title';
import Content from './Content';
import Menu from './Menu';

const Card = ({ data, count, updateItem, removeItem }) => {
    const [card, setCard] = useState({
        id: data.id,
        title: data.title,
        x: data.x,
        y: data.y,
        content: data.content,
        color: data.color,
        zIndex: count,
    });
    const [draggable, setDragabble] = useState(false);
    const [distance, setDistance] = useState({
        x: 0,
        y: 0,
    });

    const { id, x, y, title, content, color, zIndex } = card;

    const handleOnMouseDown = (e) => {
        setDistance({
            x: e.clientX - x,
            y: e.clientY - y,
        });

        setCard({
            ...card,
            zIndex: zIndex + 50,
        });

        setDragabble(true);
    };

    const handleOnMouseMove = () => {
        if (draggable) {
            const e = window.event;
            setCard({
                ...card,
                x: e.clientX - distance.x,
                y: e.clientY - distance.y,
            });
        }
    };
    const handleOnMouseUp = () => {
        setDragabble(false);
    };

    const handleColorChange = (e) => {
        setCard({
            ...card,
            color: e,
        });
    };

    const handleDraggableChange = (e) => {
        setDragabble(e);
    };

    const handleRemoveCard = () => {
        removeItem(card.id);
    };

    const handleOnMouseLeave = () => {
        setDragabble(false);
        updateItem(card);
    };

    const handleOnChange = (e) => {
        setCard({
            ...card,
            [e.target.name]: e.target.value,
        });
    };

    return (
        <div
            className={`card ${color}`}
            id={id}
            onMouseDown={handleOnMouseDown}
            onMouseMove={handleOnMouseMove}
            onMouseUp={handleOnMouseUp}
            onMouseLeave={handleOnMouseLeave}
            style={{ top: y, left: x, zIndex }}
        >
            <button type="button" className="delete" onClick={handleRemoveCard}>
                X
            </button>
            <Title
                title={title}
                handleOnChange={handleOnChange}
                handleDraggableChange={handleDraggableChange}
            />
            <Content
                content={content}
                handleOnChange={handleOnChange}
                handleDraggableChange={handleDraggableChange}
            />
            <Menu handleColorChange={handleColorChange} />
        </div>
    );
};

Card.propTypes = {
    data: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
};
const mapDispatchToProps = (dispatch) => {
    return {
        updateItem: (item) => {
            dispatch(updateItem(item));
        },
        removeItem: (id) => {
            dispatch(removeItem(id));
        },
    };
};

export default connect(null, mapDispatchToProps)(Card);
