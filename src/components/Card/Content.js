/* eslint-disable react/no-danger */
import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Content = ({ content, handleOnChange, handleDraggableChange }) => {
    const [writeText, setWriteText] = useState(false);

    const detectURL = () => {
        const urlRegex = /(((https?:\/\/)|(http?:\/\/)|(www\.))[^\s]+)/g;
        return content.replace(urlRegex, (url) => {
            return `<a href="${url}">${url}</a>`;
        });
    };
    const jsx = writeText ? (
        <textarea className="content" value={content} name="content" onChange={handleOnChange} />
    ) : (
        <p
            dangerouslySetInnerHTML={{
                __html: detectURL(content).replace(/(?:\r\n|\r|\n)/g, '<br>'),
            }}
        />
    );
    return (
        <span
            className="content"
            onMouseLeave={() => {
                setWriteText(false);
            }}
            onMouseDown={(e) => {
                e.stopPropagation();
                setWriteText(true);
                handleDraggableChange(false);
            }}
        >
            {jsx}
        </span>
    );
};
Content.propTypes = {
    content: PropTypes.string.isRequired,
    handleDraggableChange: PropTypes.func.isRequired,
    handleOnChange: PropTypes.func.isRequired,
};

export default Content;
