import axios from './instance';

export const GET_ITEMS = 'GET_ITEMS';
export const ADD_ITEM = 'ADD_ITEM';
export const UPADE_ITEM = 'UPDATE_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';

export const getItems = () => (dispatch) => {
    return axios.get('/cards.json').then((res) => {
        dispatch({
            type: GET_ITEMS,
            payload: res.data === null ? [] : res.data,
        });
    });
};

export const addItem = (item) => (dispatch) => {
    return axios.put(`/cards/${item.id}.json`, item).then(() => {
        dispatch({
            type: ADD_ITEM,
            payload: {
                type: 'cards',
                val: item,
            },
        });
    });
};
export const updateItem = (item) => (dispatch) => {
    axios.put(`/cards/${item.id}.json`, item).then(() => {
        dispatch({
            type: UPADE_ITEM,
            payload: item,
        });
    });
};

export const removeItem = (id) => (dispatch) => {
    return axios.delete(`/cards/${id}.json`).then(() => {
        dispatch({
            type: REMOVE_ITEM,
            payload: id,
        });
    });
};
