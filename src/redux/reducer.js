import { GET_ITEMS, ADD_ITEM, UPADE_ITEM, REMOVE_ITEM } from './actions';

export default (state, { type, payload }) => {
    switch (type) {
        case GET_ITEMS:
            return {
                ...state,
                cards: Object.values(payload),
            };
        case ADD_ITEM:
            return {
                ...state,
                [payload.type]: [...state[payload.type], payload.val],
            };
        case UPADE_ITEM:
            return {
                ...state,
                cards: [...state.cards].map((item) => {
                    return item.id === payload.id ? payload : item;
                }),
            };
        case REMOVE_ITEM:
            return {
                ...state,
                cards: [...state.cards].filter((card) => {
                    return card.id !== payload;
                }),
            };
        default:
            return state;
    }
};
