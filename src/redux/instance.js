import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://apitest-c0397.firebaseio.com',
    timeout: 1000,
});

export default instance;
