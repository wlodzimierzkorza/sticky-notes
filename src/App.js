import React from 'react';
import { Switch, Route } from 'react-router';
import CardBoard from './views/CardBoard';

const App = () => {
    return (
        <>
            <Switch>
                <Route strict path="/" component={CardBoard} />
            </Switch>
        </>
    );
};

export default App;
