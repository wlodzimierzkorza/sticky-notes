import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Card from '../components/Card/Card';
import AddButton from '../components/AddButton/AddButton';
import { getItems, addItem } from '../redux/actions';

const CardBoard = ({ addItem, getItems, state }) => {
    const [cards, setCard] = useState([]);

    useEffect(() => {
        getItems();
    }, [getItems]);
    useEffect(() => {
        setCard(state.cards);
    }, [state]);

    let count = 1;

    const handleCreateCard = () => {
        const width = window.innerWidth;
        const height = window.innerHeight;
        const card = {
            id: new Date().getTime(),
            x: width / 2 - 150,
            y: height / 2 - 150,
            title: 'your title',
            content: 'Your note',
            color: 'yellow',
        };
        addItem(card);
    };

    return (
        <div className="card-board">
            {cards &&
                cards.map((card) => {
                    const component = <Card data={card} key={card.id} count={count * 10} />;
                    count += 1;
                    return component;
                })}
            <AddButton onClick={handleCreateCard} />
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        state,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getItems: () => {
            dispatch(getItems());
        },
        addItem: (item) => {
            dispatch(addItem(item));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CardBoard);
